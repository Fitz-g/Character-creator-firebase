import React, { useEffect, useState } from "react";

import axios from "axios";

import Title from "../../components/Title/Title";
import Button from "../../components/Button/Button";
import Character from "../Character/Character";
import Weapons from "../Weapons/Weapons";

function CharacterCreator(props) {
  const [features, setFeatures] = useState({
    image: 1,
    force: 0,
    agility: 0,
    intelligence: 0,
    remainingPoints: 7,
    arme: null,
  });
  const [weapons, setWeapons] = useState(["arc", "epee", "hache", "fleau"]);
  const [creatorName, setCreatorName] = useState({ creatorName: "" });
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    axios
      .get(
        "https://character-creator-e1770.firebaseio.com/character-creator/weapons.json"
      )
      .then((response) => {
        const arrayWeapons = Object.values(response.data);
        setWeapons(arrayWeapons);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        console.log({ error });
      });
  }, []);

  const handleImageNext = () => {
    setFeatures((prevState) => {
      const newFeatures = { ...features };
      if (prevState.image >= 3) {
        newFeatures.image = 1;
      } else {
        newFeatures.image++;
      }
      return newFeatures;
    });
  };

  const handleImagePrev = () => {
    setFeatures((prevState) => {
      const newFeatures = { ...features };
      if (prevState.image <= 1) {
        newFeatures.image = 3;
      } else {
        newFeatures.image--;
      }
      return newFeatures;
    });
  };

  const addHandler = (carac) => {
    setFeatures((prevState, props) => {
      if (prevState[carac] >= 5 || prevState.remainingPoints <= 0)
        return features;

      const newFeaturePoint = prevState[carac] + 1;

      const copyCharacterFeatures = { ...features };
      copyCharacterFeatures.remainingPoints = prevState.remainingPoints - 1;
      copyCharacterFeatures[carac] = newFeaturePoint;

      return copyCharacterFeatures;
    });
  };
  const substractHandler = (carac) => {
    setFeatures((prevState, props) => {
      if (prevState[carac] <= 0 || prevState.remainingPoints >= 7)
        return features;

      const newFeaturePoint = prevState[carac] - 1;
      const copyCharacterFeatures = { ...features };
      copyCharacterFeatures.remainingPoints = prevState.remainingPoints + 1;
      copyCharacterFeatures[carac] = newFeaturePoint;

      return copyCharacterFeatures;
    });
  };

  const handleChooseWeapon = (weapon) => {
    const featureCharacterCopy = { ...features };
    featureCharacterCopy.arme = weapon;
    setFeatures(featureCharacterCopy);
  };

  const handleResetCharacter = () => {
    let copyCharacterFeatures = { ...features };
    copyCharacterFeatures = {
      image: 1,
      force: 0,
      agility: 0,
      intelligence: 0,
      remainingPoints: 7,
      arme: null,
    };
    setFeatures(copyCharacterFeatures);
    setCreatorName({ creatorName: "" });
  };

  const handleCreateCharacter = () => {
    setLoading(true);
    const player = {
      features: { ...features },
      creator_name: creatorName,
    };

    axios
      .post(
        "https://character-creator-e1770.firebaseio.com/character-creator/weapons.json",
        player
      )
      .then((response) => {
        console.log(response);
        setLoading(false);
        this.handleResetCharacter();
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
      });
  };

  return (
    <>
      <Title className="bg-primary text-center text-white rounded my-4 p-2" />
      <div className="form-group row mb-5 mt-3">
        <label htmlFor="creatorName" className="col-sm-2 col-form-label">
          Nom du créateur
        </label>
        <div className="col-sm-10">
          <input
            type="text"
            value={creatorName.creatorName}
            onChange={(e) => setCreatorName({ creatorName: e.target.value })}
            className="form-control"
            id="creatorName"
            placeholder="Nom du créateur"
          />
        </div>
      </div>
      <Character
        {...features}
        next={handleImageNext}
        prev={handleImagePrev}
        add={addHandler}
        substract={substractHandler}
      />
      {loading && (
        <div className="text-center fw-bold lead my-5">Loading...</div>
      )}
      {weapons && (
        <Weapons
          weapons={weapons}
          handleChooseWeapon={handleChooseWeapon}
          currentWeapon={features.arme}
        />
      )}
      <div className="d-flex gap-3 my-5 justify-content-between align-self-center flex-md-row flex-column">
        <Button className="btn-danger w-100" onClick={handleResetCharacter}>
          Réinitialiser
        </Button>
        <Button className="btn-success w-100" onClick={handleCreateCharacter}>
          Créer
        </Button>
      </div>
    </>
  );
}

export default CharacterCreator;
