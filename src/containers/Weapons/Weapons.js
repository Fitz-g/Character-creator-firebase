import React from "react";
import Weapon from "../../components/Weapon/Weapon";

import arc from "../../assets/armes/arc.png";
import epee from "../../assets/armes/epee.png";
import fleau from "../../assets/armes/fleau.png";
import hache from "../../assets/armes/hache.png";

function Weapons(props) {
  return (
    <div className="my-5 gap-2 text-center flex-wrap d-flex justify-content-between">
      {props.weapons.map((weapon) => {
        let weaponImgName;
        switch (weapon) {
          case "arc":
            weaponImgName = arc;
            break;
          case "epee":
            weaponImgName = epee;
            break;
          case "fleau":
            weaponImgName = fleau;
            break;
          case "hache":
            weaponImgName = hache;
            break;
          default:
            weaponImgName = arc;
            break;
        }
        return (
          <Weapon
            handleChooseWeapon={() => props.handleChooseWeapon(weapon)}
            key={weapon}
            weapon={weapon}
            weaponImgName={weaponImgName}
            isCurrentWeapon={props.currentWeapon === weapon}
          />
        );
      })}
    </div>
  );
}

export default Weapons;
