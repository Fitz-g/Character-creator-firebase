import React from "react";

import CharacterImage from "../../components/CharacterImage/CharacterImage";
import CaracPerso from "../CaracPerso/CaracPerso";

function Character(props) {
  return (
    <div className="row">
      <div className="col-md-6">
        <CharacterImage
          numImage={props.image}
          alt={`Image player ${props.image}`}
          next={props.next}
          prev={props.prev}
        />
      </div>
      <div className="col-md-6">
        <CaracPerso {...props} add={props.add} substract={props.substract} />
      </div>
    </div>
  );
}

export default Character;
