import React from "react";

import Feat from "../../components/Feat/Feat";

function CaracPerso(props) {
  return (
    <>
      <p>
        Point restant :{" "}
        <span className="badge bg-success">{props.remainingPoints}</span>
      </p>
      <Feat
        nbPoints={props.force}
        add={() => props.add("force")}
        substract={() => props.substract("force")}
      >
        Force
      </Feat>
      <Feat
        nbPoints={props.agility}
        add={() => props.add("agility")}
        substract={() => props.substract("agility")}
      >
        Agilité
      </Feat>
      <Feat
        nbPoints={props.intelligence}
        add={() => props.add("intelligence")}
        substract={() => props.substract("intelligence")}
      >
        Intelligence
      </Feat>
    </>
  );
}

export default CaracPerso;
