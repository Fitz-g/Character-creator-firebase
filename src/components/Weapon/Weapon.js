import React from "react";

import classes from "./weapon.module.css";

function Weapon(props) {
  return (
    <div
      style={{ opacity: props.isCurrentWeapon ? "1" : "0.5" }}
      className={`col-md-2 ${classes.weapon}`}
      onClick={props.handleChooseWeapon}
    >
      <img src={props.weaponImgName} alt={props.weapon} />
      <p className="lead mt-3">{props.weapon}</p>
    </div>
  );
}

export default Weapon;
