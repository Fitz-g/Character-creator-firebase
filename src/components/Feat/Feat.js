import React from "react";

import classes from "./feat.module.css";

function Feat(props) {
  let points = [];
  for (let i = 0; i < props.nbPoints; i++) {
    points.push(<div key={i} className={`${classes.points}`}></div>);
  }
  return (
    <div className="d-flex gap-2">
      <div
        className={`${classes.signe} ${classes.moins}`}
        onClick={props.substract}
      ></div>
      <div>{props.children}</div>
      <div
        className={`${classes.signe} ${classes.plus}`}
        onClick={props.add}
      ></div>
      {points}
    </div>
  );
}

export default Feat;
