import React from "react";

import classes from "./characterImage.module.css";

import ImagePlayer1 from "../../assets/persos/player1.png";
import ImagePlayer2 from "../../assets/persos/player2.png";
import ImagePlayer3 from "../../assets/persos/player3.png";

function CharacterImage(props) {
  let imageSrc = "";
  if (props.numImage && props.numImage === 1) {
    imageSrc = ImagePlayer1;
  } else if (props.numImage && props.numImage === 2) {
    imageSrc = ImagePlayer2;
  } else if (props.numImage && props.numImage === 3) {
    imageSrc = ImagePlayer3;
  } else {
    imageSrc = undefined;
  }

  return (
    <div className="d-flex justify-content-between align-items-center">
      <div
        className={`col-md-1 ${classes.left} ${classes.arrow}`}
        onClick={props.prev}
      ></div>
      <img
        src={imageSrc}
        alt={`${props.alt}`}
        loading="lazy"
        className={`img-fluid ${props.className ?? ""}`}
      />
      <div
        className={`col-md-1 ${classes.right} ${classes.arrow}`}
        onClick={props.next}
      ></div>
    </div>
  );
}

export default CharacterImage;
