import React from "react";

import classes from "./title.module.css";

function Title(props) {
  return (
    <h1 className={`${props.className} ${classes.title}`}>
      Créateur de personnage
    </h1>
  );
}

export default Title;
