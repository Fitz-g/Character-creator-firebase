import { useState } from "react";
import "./App.css";

import CharacterCreator from "./containers/CharacterCreator/CharacterCreator";
import Characters from "./containers/Characters/Characters";

function App() {
  const [refresh, setRefresh] = useState(false);

  const handleRefresh = () => {
    setRefresh((prevState) => {
      return !prevState;
    });
  };

  return (
    <div className="container">
      <CharacterCreator handleRefresh={handleRefresh} />
      <Characters refresh={refresh} />
    </div>
  );
}

export default App;
